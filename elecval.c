#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
int oneArg(char *arg){
		 if (strcmp("serve", arg) == 0) {
				system("ionic serve");
		 }else if(strcmp("build", arg) == 0){
			 	system("ionic build ios");
	     	system("ionic build android");
		 }else if(strcmp("help", arg) == 0){
			 	printf("Commands:\ninstall\nserve\nbuild\nrun\ncreate\nemulate\n\nSpecify \'help simulators\' for the list of iOS devices you can use with the \'emulate\' argument\n.");
		 }else if(strcmp("install", arg) == 0){
			 	system("npm install -g ionic");
			 	system("npm install -g cordova");
			 	system("npm install -g electron");
			 	system("npm install -g electron-packager");
		 }else{
			 printf("invalid argument\n");
		 }
		return 0;
}
int twoArgs(char *arg1, char *arg2){
		if (strcmp("run", arg1) == 0) {
				char electronRun[50] = "electron ";
				if(strlen(electronRun) + strlen(arg2) > 50){
					printf("\nFailed to run, make sure your project name is correct...\n");
					return 1;
				}
				strcat(electronRun, arg2);
			 	system(electronRun);
		 }else if(strcmp("serve", arg1) == 0){
			 chdir(arg2);
			 system("ionic serve");
		 }else if(strcmp("create", arg1) == 0){
			 if(strlen(arg2) > 40 || strlen(arg2) + 7 > 40){
				 printf("\nProject name is too long.(Name should be 40 characters or less)\n");
				 return 1;
			 }
			 char eprojectname[50] = "";
			 char iprojectname[50] = "";
			 strcat(eprojectname, arg2);
			 strcat(iprojectname, arg2);
			 strcat(iprojectname, "-mobile");
			 char ionicproj[62] = "ionic start ";
			 strcat(ionicproj, iprojectname);
			 system(ionicproj);
			 chdir(iprojectname);
			 char input[1];
			 printf("\nOnly use iOS if you are on OSX!\n");
			 printf("\nAdd iOS to project?(Y/n):");
			 scanf("%s", input);
			 if(strcmp("y", input) == 0){
				 	system("ionic platform add ios");
			 }
			 printf("\nAdd Android to project?(Y/n):");
			 scanf("%s", input);
			 if(strcmp("y", input) == 0){
				 	system("ionic platform add android");
			 }
			 chdir("../");
			 system("git clone https://github.com/electron/electron-quick-start");
			 char renameDef[100] = "mv electron-quick-start ";
			 strcat(renameDef, eprojectname);
			 system(renameDef);
			 chdir(eprojectname);
			 system("npm install");
		 }else if(strcmp("build", arg1) == 0){
			 if(strcmp("ios", arg2) == 0){
				 system("ionic build ios");
			 }else if(strcmp("android", arg2) == 0){
				 system("ionic build android");
			 }
		 }else if(strcmp("emulate", arg1) == 0){
			 if(strcmp("ios", arg2) == 0){
				 system("ionic emulate ios");
			 }else{
				 printf("invalid argument\n");
			 }
		 }else if(strcmp("emulate", arg1) == 0){
	 			if(strcmp("ios", arg2) == 0){
	 					system("ionic emulate ios");
	 			}
	 		}else if(strcmp("help",arg1) == 0){
				if(strcmp("simulators", arg2) == 0){
					printf("List of simulator arguments:\niOS:\niPhone-6-Plus\niPhone-6\niPhone-5s\niPhone-5\niPhone-4s\niPad-Air-2\niPad-Air\niPad-2\niPad-Retina\nResizable-iPhone\nResizable-iPad\n");
				}else{
					printf("invalid argument\n");
				}
			}else{
			 printf("invalid argument\n");
		 }
		return 0;
}
int threeArgs(char *arg1, char *arg2, char *arg3){
		if(strcmp("emulate", arg1) == 0){
			if(strcmp("ios", arg2) == 0){
					char simtype[20] = "";
					char targetselect[100] = "ionic emulate ios --target ";
					strcat(simtype, arg3);
					strcat(targetselect, simtype);
					system(targetselect);
			}
		}else{
			printf("invalid argument\n");
		}
		return 0;
}
int fiveArgs(char *arg1, char *arg2, char *arg3, char *arg4, char *arg5){
	if(strcmp("build", arg1) == 0){
		if(strcmp("osx", arg2) == 0){
			char osxbuild[100] = "electron-packager ";
			strcat(osxbuild, arg4);
			strcat(osxbuild, " ");
			strcat(osxbuild, arg5);
			strcat(osxbuild, " ");
			strcat(osxbuild, "--platform=darwin --arch=");
			strcat(osxbuild, arg3);
			system(osxbuild);
		}else if(strcmp("win", arg2) == 0 || strcmp("windows", arg2) == 0){
			char winbuild[100] = "electron-packager ";
			strcat(winbuild, arg4);
			strcat(winbuild, " ");
			strcat(winbuild, arg5);
			strcat(winbuild, " ");
			strcat(winbuild, "--platform=win32 --arch=");
			strcat(winbuild, arg3);
			system(winbuild);
		}else if(strcmp("linux", arg2) == 0){
			char linuxbuild[100] = "electron-packager ";
			strcat(linuxbuild, arg4);
			strcat(linuxbuild, " ");
			strcat(linuxbuild, arg5);
			strcat(linuxbuild, " ");
			strcat(linuxbuild, "--platform=linux --arch=");
			strcat(linuxbuild, arg3);
			system(linuxbuild);
		}else{
			printf("invalid argument\n");
		}
	}else{
		printf("invalid argument\n");
	}
		return 0;
}
int main( int argc, char *argv[] )  {
		if(argc == 2){
  		oneArg(argv[1]);
		}else if(argc == 3){
			twoArgs(argv[1], argv[2]);
		}else if(argc == 4){
			threeArgs(argv[1], argv[2], argv[3]);
		}else if(argc == 6){
			fiveArgs(argv[1], argv[2], argv[3], argv[4], argv[5]);
		}else{
			printf("Welcome to ElectroValence version 1.0.3, please refer to the readme.md included with this project!\n");
		}
		return 0;
}
