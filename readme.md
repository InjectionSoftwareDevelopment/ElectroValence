# ElectroValence
##### Author: ***Casey Erdmann***
##### Date: ***6/6/2016***
##### Version: 1.0.3
##### License: ***[LGPLv3]***
[LGPLv3]: http://www.gnu.org/licenses/lgpl-3.0.en.html
[ionic]: http://ionicframework.com/
[electron]: http://electron.atom.io/
### About:
ElectroValence is a command line program written in C that utilizes the power of [ionic] and [electron] in order to make it easy to develop applications with as few commands as possible. ElectroValence works by condensing your workflow into one project file, and allowing you to use one command set to create, run, and build your project for iOS, Android, macOS, Linux, and Windows.

### Features:
**Create**: ElectroValence allows you to instantly create what we call an "*electro app*". An *electro app* is simply a term used to describe the project that is created, but it is completely informal. What actually occurs when you create an *electro app* using ElectroValence is that the script is creating two very basic boilerplate projects for you, similarly to how you would set up a electron-quick-start app and the default blank ionic application. This makes it super convenient to create an all in one project folder for developing apps for both mobile and desktop.

**Run**: ElectroValence allows you to run your projects using very familiar commands to what you are used to with ionic. Running `elecval serve` from the mobile app directory will launch your ionic application while running `elecval run` from the desktop app directory will launch your electron application

**Emulate**: Run iOS simulator from ElectroValence, just pass it the target device you wish to emulate, or just run `elecval emulate ios` and it'll select the default for you. This will build and run your app in Apple's iOS simulator using ionic's emulate commands to build and run your cordova project. **Works on OSX only**

**Build**: Cross platform development is at the core of ElectroValence, we wanted to unify the experience of each major part of the development process, ElectroValence not only lets you create and run your projects, but it allows you to build the binaries for them too, for every platform. We've structured everything into a simple  `elecval build` command, but the structure is a little different than that of electron package and ionic's build commands based on what you are doing, read more about the specifics in our **Guide** section.


### Guide:
This guide will provide you with the instructions for how to setup and use ElectroValence's `elecval` command.
##### Setup:
As of version 1.0.3 ElectroValence provides an executable program and it's C source code via this Github repository. This program is totally open source and modifiable to meet any needs you may have. However, we have not made a convenient installer process for just the program itself yet, so it requires a little set up in order to get things up and running efficiently. Also while we have tested the binary supplied on Linux, it is actually compiled on macOS. We will be adding platform specific binaries soon, but for now if it doesn't run "OOTB" then you will have to recompile from source (which is as simple as `gcc elecval.c -o elecval`).

**THESE INSTRUCTIONS APPLY TO macOS and LINUX ONLY**. (Windows is not officially supported, although the source code can be modified and re-compiled to run on Windows).


[elecval]: https://github.com/InjectionSoftwareDevelopment/ElectroValence
+ Download [elecval]
+ Open Terminal
+ Move elecval to your systems default PATH.
    +  OSX users this is `/usr/local/bin/`
    +  Linux users this could also be `/usr/local/bin/` or  `/usr/bin/`
    +  If you are unsure of your path type `echo $PATH` into terminal and you will find more information
+ Run `elecval` in terminal if you see the following message you have set everything up correctly:
    + "Welcome to ElectroValence version 1.0.2, please refer to the readme.md included with this project!"

Run `elecval help` for a list of usable commands.


If you run into issues running any commands, you may not have the proper node packages installed. Run `elecval install` to fix this. This command will install the proper packages for ionic, cordova, and electron. **If you don't already have NodeJS installed this won't work.**

##### Creating a project:

Now that you have installed ElectroValence it's time to create our first project! To start, simply create a folder where you would like to house your project, but keep in mind, although ElectroValence makes integration between ionic and electron easy, they are still two seperate projects, and as a result each has their own folder. A good example project folder set up would be:

    Project Folder:
        Myproject-mobile
        Myproject

In this example *Project Folder* is the folder you would create, and *Myproject* on the two inner folder names is the name of your project.

To create a project:

+ In terminal, navigate to the folder you created
+ Run `elecval create <project name>`

That's it! During the script you will be prompted about adding iOS and Android to the ionic platform setup, but other than that once the script finishes execution you are ready to start coding!

##### Executing/Running a project:

Running an ionic project is a little different than running an electron project, but the process is actually familiar if you are used to running either of them separately.

To run an ionic project:
+ In terminal, navigate to your project folder
+ Run `elecval serve <ionic app folder>` alternatively if you are already inside of the ionic app folder rather than the project folder just run `elecval serve`

To run an electron project:
+ In terminal, navigate to your project folder
+ Run `elecval run <electron app folder>`

That's all there is to it! Using those commands should either pull up a browser featuring your ionic project or run your electron app.

##### Running iOS simulator:

ElectroValence can build and run your ionic app in iOS simulator using ionic's emulate commands. We have made the syntax for specifying a target device even simpler, and also added a list of default devices via our `help simulators` command.

To run an ionic project in iOS simulator:
+ In terminal, navigate to your project folder
+ Run `elecval emulate ios` or `elecval emulate ios <target device name>`

This will build and run your mobile app inside of iOS simulator, for a list of all target devices just type `elecval help simulators`. Keep in mind this command works on ***OSX Only!!!***

##### Building a project:

Being able to build a project in one area is convenient, and we have also taken the liberty of making the build commands themselves a little easier. Similar to running the apps, the commands for building apps for ionic and electron are a little different.

To build an ionic mobile app:
+ In terminal, navigate to the ionic mobile app directory inside of your project
+ Run `elecval build` to build both the iOS and Android app for your project, alternatively run `elecval build ios` or `elecval build android` to build them separately.

To build an electron desktop app:
+ In terminal, navigate to your project directory
+ Run the command `elecval build <OS> <Architecture> <Electron App Directory> <Electron App Name>`
    + An example to build an OSX application would be `elecval build osx x64 my-app MyApp`
    + To build for Linux replace `osx` with `linux`. For Windows replace it with `win` or `windows`
    + Architecture options are `x64` and `ia32`

[Injection]: https://injecti0n.org/
#### ElectroValence is a product of [Injection], visit our site for more cool software!
